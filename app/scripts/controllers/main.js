'use strict';

/**
 * @ngdoc function
 * @name bitbucketLabsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bitbucketLabsApp
 */
angular.module('bitbucketLabsApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
