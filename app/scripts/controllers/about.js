'use strict';

/**
 * @ngdoc function
 * @name bitbucketLabsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bitbucketLabsApp
 */
angular.module('bitbucketLabsApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
